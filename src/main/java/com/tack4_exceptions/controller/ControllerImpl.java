package com.tack4_exceptions.controller;

import com.tack4_exceptions.model.BusinessLogic;
import com.tack4_exceptions.model.Model;
import com.tack4_exceptions.model.Plane;

import java.util.ArrayList;

/**
 * Controller <- Model </br>
 * Controller -> Model </br>
 * Controller <- View </br>
 * Controller -> View </br>
 */
public class ControllerImpl implements Controller {
    private Model model;

    /**
     * Constructor to create new model
     */
    public ControllerImpl() {
        model = new BusinessLogic();
    }

    /**
     * @param planeArrayList set list of planes
     */
    @Override
    public void setPlaneArrayList(ArrayList<Plane> planeArrayList) {
        model.setPlaneArrayList(planeArrayList);
    }

    /**
     * @return list of planes
     */
    @Override
    public ArrayList<Plane> getPlaneArrayList() {
        return model.getPlaneArrayList();
    }
}
