package com.tack4_exceptions.model;


import java.util.ArrayList;

/**
 * Controller <- Model </br>
 * Controller -> Model </br>
 */
public class BusinessLogic implements Model {

    private Domain domain;

    /**
     * Constructor to create new domain
     */
    public BusinessLogic(){
        domain = new Domain();
    }

    /**
     * @return planes list
     */
    @Override
    public ArrayList<Plane> getPlaneArrayList() {
        return domain.getPlaneArrayList();
    }

    /**
     * @param planeArrayList set planes list
     */
    @Override
    public void setPlaneArrayList(ArrayList<Plane> planeArrayList) {
        domain.setPlaneArrayList(planeArrayList);
    }
}
